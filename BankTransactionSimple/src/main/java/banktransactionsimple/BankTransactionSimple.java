package banktransactionsimple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.List;


public class BankTransactionSimple {
    private static final String RESOURCES = "src/main/resources/";

    public static void main(String[] args) throws IOException {
        final Path path = Paths.get(RESOURCES + "bank-data-simple.csv");
        final List<String> lines = Files.readAllLines(path);

        totalForAll(lines);

        totalForMonth(lines);

    }

    private static void totalForMonth(List<String> lines) {
        double total = 0d;
        final DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        for (String line : lines) {
            final String[] split = line.split(",");
            final LocalDate date = LocalDate.parse(split[0], DATE_PATTERN);
            if (date.getMonth() == Month.JANUARY) {
                final double amount = Double.parseDouble(split[1]);
                total += amount;
            }
        }
        System.out.println("The total for all transactions in January is " + total);
    }

    private static void totalForAll(List<String> lines) {
        double total = 0d;
        for (String line : lines) {
            final String[] split = line.split(",");
            final double amount = Double.parseDouble(split[1]);
            total += amount;
        }
        System.out.println("The total for all transactions is " + total);
    }
}
