package banktransaction;

import java.time.LocalDate;

public class BankTransactionTestDrive {
    public static void main(String[] args) {
        BankTransaction bankTransaction = new BankTransaction(LocalDate.now(),
                100, "Пятерочка");

        System.out.println(bankTransaction.toString());
    }
}
